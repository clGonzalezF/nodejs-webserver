const express = require('express');
const app = express();

app.get('/', function(req, res) {
    let salida = {
        nombre: 'Sakura',
        edad: 20,
        url: req.url
    }
    res.send(salida);
    //res.send('Hello World nodejs');
});

app.listen(3000, () => {
    console.log('Escuchando peticiones en puerto 3000');
});