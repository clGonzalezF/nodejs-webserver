const express = require('express');
const app = express();
const hbs = require('hbs');
require('./hbs/helpers');

const port = process.env.PORT || 3000;

app.use(express.static(__dirname + '/public'));

// Express HBS engine
hbs.registerPartials(__dirname + '/views/partials');
app.set('view engine', 'hbs');


app.get('/', function(req, res) {
    res.render('home', { nombre: 'Sakura', anio: new Date().getFullYear() });
    //res.send('Hello World nodejs');
});

app.get('/about', function(req, res) {
    res.render('about');
});

app.get('/json', function(req, res) {
    let salida = {
        nombre: 'Sakura',
        edad: 20,
        url: req.url
    }
    res.send(salida);
    //res.send('Hello World nodejs');
});

app.listen(port, () => {
    console.log(`Escuchando peticiones en puerto ${ port }`);
});